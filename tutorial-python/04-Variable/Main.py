# variabel adalah tempat menyimpan data
# menaruh / assigment data

a = 10
x = 5
panjang = 1000

#pemanggilan pertama
print ("Nilai x = ", x)
print ("Nilai a = ", a)
print ("Nilai panjang = ", panjang)

# Penamaan
nilai_y = 15 # dengan mengunakan underscore
juta10 = 10000000 #ini boleh
nilaiZ = 17.5 # ini boleh

#pemanggilan ke dua
print ("Nilai a = ", a)
a = 7
print ("Nilai a = ", a)

#assigment indirect
b = a
print ("Nilai b = ", b)