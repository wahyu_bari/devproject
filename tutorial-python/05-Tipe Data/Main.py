# a = 10 dimana a adalah varabel dengan nilai 10

#tipe data : Angka satuan (integer) yang gak ada koma nya
data_integer = 11
print("data : ", data_integer)
print("- bertipe : ", type (data_integer))

# tipe data : angka dengan koma (float)
data_float = 1.5
print("data : ", data_float)
print("- bertipe : ", type (data_float))

# tipe data : kumulan karakter ( string)
data_string = "ucup 11"
print("data : ", data_string)
print("- bertipe : ", type (data_string))

# tipe data : biner true/false (boolean)
data_bool = True
print("data : ", data_bool)
print("- bertipe : ", type (data_bool))

# tipe data khusus
# tipe data complex
data_complex = complex(5,6)
print("data : ", data_complex)
print("- bertipe : ", type (data_complex))

# tipe data dari bahasa c
from ctypes import c_double
data_C_double = c_double(10.5)
print("data : ", data_C_double)
print("- bertipe : ", type (data_C_double))