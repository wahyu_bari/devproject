# Operasi Aritmatika

a = 10
b = 3

#operasi penambahan +
hasil = a + b
print(a,'+',b,'=', hasil)

#operasi pengurangan -
hasil = a - b
print(a,'-',b,'=', hasil)

#operasi perkali *
hasil = a * b
print(a,'*',b,'=', hasil)

#operasi pembagian /
hasil = a / b
print(a,'/',b,'=', hasil)

#operasi eksponen (pangkat) **
hasil = a ** b
print(a,'**',b,'=', hasil)

#operasi modulus (sisa pembagian) %
hasil = a % b
print(a,'%',b,'=', hasil)

#operasi floor devision (kebalikan dari modulus) // (dibulatkan dari pembagian)
hasil = a // b
print(a,'//',b,'=', hasil)


#prioritas operasi, urutan (),exponen,perkalian,pembagian,penambahan,pengurangan
'''
    1. ()
    2. exponen **
    3. perkalian dan teman teman * / ** % //
    4. penambahan dan pengurangan + -
'''

x = 3
y = 2
z = 4

hasil = x ** y * (z + x) / y - y % z // x #sesuai dengan urutan
print(x,'**',y,'*',z,'+',x,'/',y,'-',y,'%',z,'//',x,'=',hasil)

hasil = x + y * z #sesuai dengan urutan
print(x,'+',y,'*',z,'=',hasil)

hasil = (x + y) * z #kurung akan mengambil langkah paling pertama
print('(',x,'+',y,')*',z,'=',hasil)

